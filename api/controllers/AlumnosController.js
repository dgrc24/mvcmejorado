/**
 * AlumnosController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  mostrar: function (req, res) {
    Alumnos.find({}).exec(function (err, alumnos) {
      if (err) {
        res.send(500, { error: "Database Error" });
      }
      res.view("mostrar", { alumnos: alumnos });
    });
  },
  agregar: function (req, res) {
    res.view("agregar");
  },
  crear: function (req, res) {
    var nc = req.body.nc;
    var nombre = req.body.nombre;
    var apellido = req.body.apellido;
    var semestre = req.body.semestre;
    var grupo = req.body.grupo;
    var domicilio = req.body.domicilio;
    var fechaNac = req.body.fechaNac;
    var telefono = req.body.telefono;

    Alumnos.crear({
      nc: nc,
      nombre: nombre,
      apellido: apellido,
      semestre: semestre,
      grupo: grupo,
      domicilio: domicilio,
      fechaNac: fechaNac,
      telefono: telefono,
    }).exec(function (err) {
      if (err) {
        res.send(500, { error: "Database Error" });
      }

      res.redirect("/alumnos/mostrar");
    });
  },
};
